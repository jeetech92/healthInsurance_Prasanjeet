package com.healthinsurance.test;

import com.healthinsurance.currenthealth.CurrentHealth;
import com.healthinsurance.details.Details;
import com.healthinsurance.habits.Habits;
import com.healthinsurance.patientsdetails.PatientsDetails;

public class Test {

	public static void main(String[] args) {
		PatientsDetails pd=new PatientsDetails();
		Details dls=new Details();
		CurrentHealth ch=new CurrentHealth();
		Habits hb=new Habits();
		dls.setName("Norman Gomes");
		dls.setAge(34);
		dls.setGender("male");
		ch.setBlood_pressure("no");
		ch.setBlood_sugar("no");
		ch.setHypertension("no");
		ch.setOverweigh("yes");
		hb.setAlcohol("yes");
		hb.setDaily_exercise("yes");
		hb.setDrugs("no");
		hb.setSmoking("no");
		System.out.println(pd.getInsurance(dls, ch, hb));
	
		
	}

}
