package com.healthinsurance.patientsdetails;

import com.healthinsurance.currenthealth.CurrentHealth;
import com.healthinsurance.details.Details;
import com.healthinsurance.habits.Habits;

public class PatientsDetails {
	
	public int getInsurance(Details dtls,CurrentHealth ch,Habits hb)
	{
		int insurance=5000,count=0,ex=0,bd=0,ge=0,gd=0;
		int age=dtls.getAge();
		if(dtls.getAge()>=18)
		{
			if(dtls.getAge()>=18 && dtls.getAge()<=25)
			{
				insurance=insurance+insurance*10/100;
			}
			else if(dtls.getAge()>=25 && dtls.getAge()<=40)
			{
				while(age>=25)
				{
					age=age-5;
					insurance=insurance+insurance*10/100;
				}
				insurance=insurance+insurance*10/100;
				
			}
			else
			{
				while(age>40)
				{
					insurance=insurance+insurance*20/100;
					age=age-5;
				}
				while(age>=25)
				{
					insurance=insurance+insurance*20/100;
					age=age-5;
				}
				insurance=insurance+insurance*10/100;
				//age above 40
			}
		}//age if
		else
		{
			return insurance;
		}
		
		if(dtls.getGender().equals("male"))
		{
			insurance=insurance+insurance*2/100;//increase 2%
			
		}
		if(ch.getBlood_pressure().equals("yes")||ch.getBlood_sugar().equals("yes")||ch.getHypertension().equals("yes")||ch.getOverweigh().equals("yes"))
		{
			insurance=insurance+insurance*1/100;//increase 1%
		
		}
		
		if(hb.getAlcohol().equals("yes")||hb.getSmoking().equals("yes")||hb.getDrugs().equals("yes"))
		{
			insurance=insurance+insurance*3/100;//increase 3%
		
		}
		if(hb.getDaily_exercise().equals("yes"))
		{
			insurance=insurance-insurance*3/100;//reduce 3%
			
		}
		
		return insurance;
	}

}
