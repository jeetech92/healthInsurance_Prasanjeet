package com.healthinsurance.currenthealth;

public class CurrentHealth {
	private String hypertension;
	private String blood_pressure;
	private String blood_sugar;
	private String overweigh;
	public String getHypertension() {
		return hypertension;
	}
	public void setHypertension(String hypertension) {
		this.hypertension = hypertension;
	}
	public String getBlood_pressure() {
		return blood_pressure;
	}
	public void setBlood_pressure(String blood_pressure) {
		this.blood_pressure = blood_pressure;
	}
	public String getBlood_sugar() {
		return blood_sugar;
	}
	public void setBlood_sugar(String blood_sugar) {
		this.blood_sugar = blood_sugar;
	}
	public String getOverweigh() {
		return overweigh;
	}
	public void setOverweigh(String overweigh) {
		this.overweigh = overweigh;
	}
	
	
	

}