package com.healthinsurance.habits;

public class Habits {
	private String smoking;
	private String alcohol;
	private String daily_exercise;
	private String drugs;
	public String getSmoking() {
		return smoking;
	}
	public void setSmoking(String smoking) {
		this.smoking = smoking;
	}
	public String getAlcohol() {
		return alcohol;
	}
	public void setAlcohol(String alcohol) {
		this.alcohol = alcohol;
	}
	public String getDaily_exercise() {
		return daily_exercise;
	}
	public void setDaily_exercise(String daily_exercise) {
		this.daily_exercise = daily_exercise;
	}
	public String getDrugs() {
		return drugs;
	}
	public void setDrugs(String drugs) {
		this.drugs = drugs;
	}
	
	
}
