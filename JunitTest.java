package com.healthinsurance.junit;

import static org.junit.Assert.*;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import com.healthinsurance.currenthealth.CurrentHealth;
import com.healthinsurance.details.Details;
import com.healthinsurance.habits.Habits;
import com.healthinsurance.patientsdetails.PatientsDetails;

import junit.framework.TestCase;

public class JunitTest extends TestCase{

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		
		
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Test
	public void test() {
		Details dls=new Details();
		CurrentHealth ch=new CurrentHealth();
		Habits hb=new Habits();
		dls.setName("Norman Gomes");
		dls.setAge(34);
		dls.setGender("male");
		ch.setBlood_pressure("no");
		ch.setBlood_sugar("no");
		ch.setHypertension("no");
		ch.setOverweigh("yes");
		hb.setAlcohol("yes");
		hb.setDaily_exercise("yes");
		hb.setDrugs("no");
		hb.setSmoking("no");
		MyJunitTest jtest=new MyJunitTest();
		int result=jtest.getInsurance(ch, dls, hb);
		assertEquals(6849,result);
	}

}
